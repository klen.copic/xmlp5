<?php echo validation_errors(); ?>

<?php echo form_open('news/edit'); ?>

    <label for="title">Title</label>
    <input type="input" name="title" value="<?php echo $news_item['title']; ?>" /><br />

	<input type="input" name="slug" value="<?php echo $news_item['slug']; ?>" /><br />
    <label for="text">Text</label>
    <textarea name="text"><?php echo $news_item['text']; ?></textarea><br />

    <input type="submit" name="submit" value="Save news item" onClick="return confirm('Are you sure you want to edit?');" />

</form>