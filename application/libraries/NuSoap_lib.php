<?php
class NuSoap_lib{   

    public function __construct(){
   		require_once("lib/nusoap.php");
    }

    //PREVERI ZAKAJ NE DELA!
    /*public function getWeather($lat, $lon)
    {
        $client = new nusoap_client("https://graphical.weather.gov:443/xml/SOAP_server/ndfdXMLserver.php", false);

	    $err = $client->getError();
	    if ($err) {
	        echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
	        echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
	        exit();
	    }

	    $weatherParameters = array(
	        'maxt' => "1",
	        'mint' => "1",
	        'temp' => "1",
	        'td' => "0",
	        'pop12' => "0",
	        'qpf' => "0",
	        'sky' => "0",
	        'snow' => "0",
	        'wspd' => "0",
	        'wdir' => "0",
	        'wx' => "0",
	        'waveh' => "0",
	        'icons' => "1",
	        'rhm' => "0",
	        'apt' => "0",
	        'tcwspdabv34i' => "0",
	        'tcwspdabv50i' => "0",
	        'tcwspdabv64i' => "0",
	        'tcwspdabv34c' => "0",
	        'tcwspdabv50c' => "0",
	        'tcwspdabv64c' => "0",
	        'conhazo' => "0",
	        'ptornado' => "0",
	        'phail' => "0",
	        'ptstmwinds' => "0",
	        'pxtornado' => "0",
	        'pxhail' => "0",
	        'pxtstmwinds' => "0",
	        'ptotsvrtstm' => "0",
	        'ptotxsvrtstm' => "0",
	        'tmpabv14d' => "0",
	        'tmpblw14d' => "0",
	        'tmpabv30d' => "0",
	        'tmpblw30d' => "0",
	        'tmpabv90d' => "0",
	        'tmpblw90d' => "0",
	        'prcpabv14d' => "0",
	        'prcpblw14d' => "0",
	        'prcpabv30d' => "0",
	        'prcpblw30d' => "0",
	        'prcpabv90d' => "0",
	        'prcpblw90d' => "0",
	        'precipa_r' => "0",
	        'sky_r' => "0",
	        'td_r' => "0",
	        'temp_r' => "0",
	        'wdir_r' => "0",
	        'wspd_r' => "0",
	        'wgust' => "0"
	    );

	    $params = array(
	        'latitude' => $lat,
	        'longitude'=> $lon,
	        'product'=> 'time-series',
	        'startTime'=> '2020-05-11T08:00:00',
	        'endTime'=> '2020-05-15T08:00:00',
	        'Unit'=> 'm',
	        'weatherParameters'=> $weatherParameters,

	    );

	    $result = $client->call('NDFDgen', $params, '', '');
		print_r($result); echo "test"; die;
	    return simplexml_load_string($result);
    }*/

    public function getWeather($lat, $lon){

		$client = new nusoap_client("https://graphical.weather.gov:443/xml/SOAP_server/ndfdXMLserver.php", false);

		$err = $client->getError();
		if ($err) {
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
			exit();
		}

		$weatherParameters = array(
			'maxt' => "1" ,
			'mint' => "1" ,
			'temp' => "1" ,
			'dew' => "0" ,
			'pop12' => "0" ,
			'qpf' => "0" ,
			'sky' => "0" ,
			'snow' => "0" ,
			'wspd' => "0" ,
			'wdir' => "0" ,
			'wx' => "0" ,
			'waveh' => "0" ,
			'icons' => "1" ,
			'rh' => "0" ,
			'appt' => "0" ,
			'incw34' => "0" ,
			'incw50' => "0" ,
			'incw64' => "0" ,
			'cumw34' => "0" ,
			'cumw50' => "0" ,
			'cumw64' => "0" ,
			'critfireo' => "0" ,
			'dryfireo' => "0" ,
			'conhazo' => "0" ,
			'ptornado' => "0" ,
			'phail' => "0" ,
			'ptstmwinds' => "0" ,
			'pxtornado' => "0" ,
			'pxhail' => "0" ,
			'pxtstmwinds' => "0" ,
			'ptotsvrtstm' => "0" ,
			'pxtotsvrtstm' => "0" ,
			'tmpabv14d' => "0" ,
			'tmpblw14d' => "0" ,
			'tmpabv30d' => "0" ,
			'tmpblw30d' => "0" ,
			'tmpabv90d' => "0" ,
			'tmpblw90d' => "0" ,
			'prcpabv14d' => "0" ,
			'prcpblw14d' => "0" ,
			'prcpabv30d' => "0" ,
			'prcpblw30d' => "0" ,
			'prcpabv90d' => "0" ,
			'prcpblw90d' => "0" ,
			'precipa_r' => "0" ,
			'sky_r' => "0" ,
			'temp_r' => "0" ,
			'td_r' => "0" ,
			'wdir_r' => "0" ,
			'wspd_r' => "0" ,
			'wwa' => "0" ,
			'wgust' => "0" ,
			'iceaccum' => "0"
		);
		
		

		$params = array(
			'latitude' => $lat,
			'longitude'=> $lon,
			'product'=> 'time-series',
			'startTime'=> '2020-05-11T08:00:00',
			'endTime'=> '2020-05-15T08:00:00',
			'Unit'=> 'm',
			'weatherParameters'=> $weatherParameters
		);
		$result = $client->call('NDFDgen', $params, '', '');
		return simplexml_load_string($result);
	}
}
?>
