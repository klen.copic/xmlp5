<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~klen/ci_nusoap_mon/codeignitor/assets/css/mysyle.css" />
      </head>
      <body>
        <xsl:for-each select="forecast/data">
          <h2>Weather</h2>
          <table>
            <tr bgcolor="#9acd32">
              <th style="text-align:left">Time</th>
              <th style="text-align:left">Icon</th>
              <th style="text-align:left">Temperature</th>
              <th style="text-align:left">Temperature Min</th>
              <th style="text-align:left">Temperature Max</th>
            </tr>
              <tr>
                <td>
                  <xsl:for-each select="time-layout[layout-key='k-p3h-n26-3']">
                    <xsl:for-each select="start-valid-time">
                      <div><xsl:value-of select="." /></div>
                    </xsl:for-each>    
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="parameters/conditions-icon">
                    <xsl:for-each select="icon-link">
                        <div><img src="{.}"/></div>
                    </xsl:for-each>    
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="parameters/temperature[@type='hourly']">
                    <xsl:for-each select="value">
                        <div><xsl:value-of select="." /></div>
                    </xsl:for-each>    
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="parameters/temperature[@type='minimum']">
                    <xsl:for-each select="value">
                        <div><xsl:value-of select="." /></div>
                    </xsl:for-each>    
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="parameters/temperature[@type='maximum']">
                    <xsl:for-each select="value">
                        <div><xsl:value-of select="." /></div>
                    </xsl:for-each>    
                  </xsl:for-each>
                </td>
              </tr>
          </table>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>